<?php


use Illuminate\Support\Facades\Route;

Route::group(
    [
        'namespace'  => 'KDA\Shop\Customer\Http\Controllers',
        'middleware' => ['web','auth:customers'],
        'prefix'     => config('kda.webshop.routes.prefix'),
    ],
    function () {

        Route::group([  'prefix'     => config('kda.webshop.routes.dashboard')],
        function(){
            $route = config('kda.shop.cam.route_addr','/dashboard/addresses');
            Route::get($route,'AddressController@index')->name('cam_dashboard');
            Route::get($route.'/pick','AddressController@pick')->name('cam_pick');
            Route::get($route.'/delegated','AddressController@delegated')->name('cam_delegated');
            Route::get($route.'/{id}/picked','AddressController@picked')->name('cam_picked');

            Route::get($route.'/create','AddressController@create')->name('cam_create');
            Route::post($route,'AddressController@store')->name('cam_store');

            Route::get($route.'/{id}/edit','AddressController@edit')->name('cam_edit');;
            Route::post($route.'/{id}','AddressController@update')->name('cam_update');

            Route::get($route.'/{id}/delete','AddressController@destroy')->name('cam_destroy');
        });

      
    }
);