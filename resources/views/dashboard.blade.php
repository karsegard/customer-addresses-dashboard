<div class="container">
    <h1>
        @if($pick==false)
        Gérer vos adresses
        @else
        Choisir une adresse
        @endif
    </h1>
    <div class="row justify-content-center">
        <ul>
            @if ($status = session()->get('error'))
                <div class="error">
                    {{ $status }}
                </div>
            @endif
            @if ($addresses)
                @foreach ($addresses as $address)
                    <li>
                        {!! $address->full_address !!}

                        <a href="{{ route('cam_destroy', ['id' => $address->id]) }}">supprimer</a>
                        <a href="{{ route('cam_edit', ['id' => $address->id]) }}">modifier</a>
                        @if($pick==true)
                        <a href="{{ route('cam_picked', ['id' => $address->id]) }}">selectionner cette adresse</a>
                        @endif
                    </li>
                @endforeach
            @endif
            <a href="{{ route('cam_create') }}">ajouter</a>
        </ul>

    </div>
</div>
