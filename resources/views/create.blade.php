<form method="POST" action="{{ route('cam_store') }}">
    @csrf
    <div class="field">
        <label class="field__label">Firstname</label>
        <div class="field__field">
            <input type="text" name="firstname" />

            @error('firstname')
                <p class="error">
                    <span class="error"></span>
                    {{ $message }}
                </p>
            @enderror
        </div>
        <label class="field__label">Lastname</label>

        <div class="field__field">
            <input type="text" name="lastname" />

            @error('lastname')
                <p class="error">
                    <span class="error"></span>
                    {{ $message }}
                </p>
            @enderror
        </div>
        <label class="field__label">Street</label>

        <div class="field__field">
            <input type="text" name="street" />

            @error('street')
                <p class="error">
                    <span class="error"></span>
                    {{ $message }}
                </p>
            @enderror
        </div>
        <label class="field__label">Street2</label>

        <div class="field__field">
            <input type="text" name="street2" />

            @error('street2')
                <p class="error">
                    <span class="error"></span>
                    {{ $message }}
                </p>
            @enderror
        </div>
        <label class="field__label">ZIP</label>

        <div class="field__field">
            <input type="text" name="zip" />

            @error('zip')
                <p class="error">
                    <span class="error"></span>
                    {{ $message }}
                </p>
            @enderror
        </div>
        <label class="field__label">City</label>

        <div class="field__field">
            <input type="text" name="city" />

            @error('city')
                <p class="error">
                    <span class="error"></span>
                    {{ $message }}
                </p>
            @enderror
        </div>

        <div class="field__field">
            <select id="country_id" type="text"
                class="form-control @error("country") is-invalid @enderror" name="country_id"
                value="{{ old("country_id") }}" required autocomplete="country">
                @foreach ($countries as $country)
                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                @endforeach
            </select>
            @error('country_id')
                <p class="error">
                    <span class="error"></span>
                    {{ $message }}
                </p>
            @enderror
        </div>
    </div>



    <button type="submit">Créer</button>
</form>
