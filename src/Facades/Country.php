<?php 
namespace KDA\Shop\Customer\Facades;

use Illuminate\Support\Facades\Facade;


class Country extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return config('kda.shop.cam.country_model'); }
}