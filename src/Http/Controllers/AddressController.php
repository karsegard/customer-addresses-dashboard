<?php

namespace KDA\Shop\Customer\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use KDA\Shop\Customer\Facades\Address;
use KDA\Shop\Customer\Facades\Country;
use Illuminate\Support\Facades\Validator;

class AddressController extends Controller
{

    use \KDA\Shop\Customer\Library\Auth\CustomerAuthGuard;
    use \KDA\Shop\Customer\Library\Auth\CustomerRedirect;

    
    protected function addressValidator(array $data)
    {

        // $additionnal_validator = config('kda.webshop.customers.register.validation',[]);
        // $messages = config('kda.webshop.customers.register.messages',[]);
        $validator =  Validator::make($data, [
            'lastname' => ['required', 'string', 'max:255'],
            'firstname' => ['required', 'string', 'max:255'],
            'street' => ['required', 'string', 'max:255'],
            'zip' => ['required', 'string', 'min:4,max:4'],
            'city' => ['required', 'string', 'max:255'],

        ]);

        /*$validator->after(function($validator) use ($data){
            $this->checkIfUserAttributeExists($data,$validator);
        });*/
        return $validator;
    }

    public function index(){
        $addr = Address::active()->forCustomer(webshop_user())->get();
        delegate('cam.store','cam_dashboard');
        delegate('cam.update','cam_dashboard');
        delegate('cam.delete','cam_dashboard');
        return view('kda/shop/cam::dashboard',['addresses'=>$addr,'pick'=>false]);
    }

    public function create(){
        return view('kda/shop/cam::create',[
            'countries'=> Country::all()
        ]);
    }

    public function store(Request $request){
        $this->addressValidator($request->all())->validate();
        Address::create(array_merge($request->all(),['customer_id'=>webshop_user()->id]));
        return $this->redirectTo('cam.store','cam_dashboard');
    }

    public function edit($id){
        $address=  Address::forCustomer(webshop_user())->where('id',$id)->first();
        if(!$address->canBeModified){
            //session()->flash('error','L\'adresse est associée à une commande et ne peut plus être modifiée');
            //return back();
            $newaddress = $address->replicate();
            $newaddress->save();
            $address->deleted_by_user=true;
            $address->save();
            $address= $newaddress;
        }
        if(!$address){
            return back();
        }
        return view('kda/shop/cam::edit',[
            'countries'=> Country::all(),
            'address'=> $address
        ]);
    }

    
    public function update(Request $request, $id){
        $this->addressValidator($request->all())->validate();
        Address::find($id)->update(array_merge($request->all(),['customer_id'=>webshop_user()->id]));
        return $this->redirectTo('cam.update','cam_dashboard');
    }

    public function destroy($id){
        if($id){
            $addr = Address::forCustomer(webshop_user())->where('id',$id)->first();
            $addr->deleted_by_user = true;
            $addr->save();
        }
        return $this->redirectTo('cam.delete','cam_dashboard');
    }

    public function pick(){
        $error =$status = session()->get('error');
       
        delegate('cam.store','cam_pick');
        delegate('cam.update','cam_pick');
        delegate('cam.delete','cam_pick');
        session()->flash('error',$error);
        $addr = Address::active()->forCustomer(webshop_user())->get();
        return view('kda/shop/cam::dashboard',['addresses'=>$addr,'pick'=>true]);
    }

    public function delegated(){
        $addr = Address::active()->forCustomer(webshop_user())->get();
        return view('kda/shop/cam::dashboard',['addresses'=>$addr,'pick'=>false]);
    }
    
    public function picked($id){
        return $this->redirectTo('cam.picked','cam_pick',['id'=>$id]);
    }


    public function redirectTo($key='cam.picked',$default='cam_pick',$args=[]){
        
        return redirect(route(get_delegate($key,$default),$args));
    }
    
}
