<?php

namespace KDA\Shop\Customer;

use KDA\Laravel\PackageServiceProvider;


class AddressServiceProvider extends PackageServiceProvider
{

    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasHelper;

    protected $additionnalProviders = [
        EventServiceProvider::class
    ];
    
    protected $routes = [
        'cam.php'
    ];
   
    

    protected $configs = [
        'kda/cam.php'=> 'kda.shop.cam'
    ];

    protected $viewNamespace = 'kda/shop/cam';
    protected $publishViewsTo = 'vendor/kda/shop/cam';
     
 
    protected function packageBaseDir () {
        return dirname(__DIR__,1);
    }

    

    public function boot (){
       // $this->loadHelper('auth.php');
        
        parent::boot();
    }
   
}
