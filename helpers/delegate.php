<?php



if (!function_exists('delegate')) {
    /*
     * Returns the name of the guard defined
     * by the application config
     */
    function delegate($key, $route)
    {
        \Session::put('kda_delegate.' . $key, $route);
        \Session::save();
    }
}
if (!function_exists('get_delegate')) {
    function get_delegate($key, $default = NULL)
    {
        $result = \Session::pull('kda_delegate.' . $key);
        \Session::save();

        return $result ?? $default;
    }
}
